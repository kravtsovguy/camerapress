//
//  MediaViewController.swift
//  CameraPress
//
//  Created by Matvey Kravtsov on 02.07.16.
//  Copyright © 2016 Matvey Kravtsov. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MediaViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var playImage: UIImageView!
    
    var image: UIImage = UIImage()
    var path: NSURL = NSURL()
    var isVideo: Bool {
        get{ return path.absoluteString != NSURL().absoluteString }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(MediaViewController.playVideoPressed(_:)))
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        playImage.hidden = !isVideo
        
        imageView.image = image
        setFirstFrame()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

    }
    
    
    func setup(image:UIImage, path:NSURL){
        
        self.image = image
        self.path = path
        
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    
    @IBAction func savePressed(sender: AnyObject) {
        
        if isVideo {
            UISaveVideoAtPathToSavedPhotosAlbum(path.path!, nil, nil, nil)
        }else{
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        
        
        let alert = UIAlertController(title: "Сохранено в галерее", message: "", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel){ (act) in
            self.cancelPressed(self)
        })
        presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func playVideoPressed(sender: AnyObject) {
        playVideo()
    }
    
    private func setFirstFrame(){
        if !isVideo {return}
        
        let asset = AVURLAsset(URL: path)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let image = try! UIImage(CGImage: imageGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil), scale: 1.0, orientation: .Right)
        
        imageView.image = image
    }
    
    private func playVideo() {
        if !isVideo {return}

        let player = AVPlayer(URL: path)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.presentViewController(playerController, animated: true) {
            player.play()
        }
    }
    

}
