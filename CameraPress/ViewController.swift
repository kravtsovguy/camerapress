//
//  ViewController.swift
//  CameraPress
//
//  Created by Matvey Kravtsov on 02.07.16.
//  Copyright © 2016 Matvey Kravtsov. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var capturedImage = UIImage()
    var pathVideo = NSURL()
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var mainButton: UIView!
    
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var isCapturingVideo = false;
    var videoFileOutput: AVCaptureMovieFileOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetHigh
        
        
        let backCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let audioDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio)
        
        var error: NSError?
        var videoInput: AVCaptureDeviceInput!
        var audioInput: AVCaptureDeviceInput!
        do {
            videoInput = try AVCaptureDeviceInput(device: backCamera)
            audioInput = try AVCaptureDeviceInput(device: audioDevice)
        } catch let err as NSError {
            error = err
            videoInput = nil
            audioInput = nil
        }
        
        if error == nil && captureSession!.canAddInput(videoInput) && captureSession!.canAddInput(audioInput) {
            captureSession!.addInput(videoInput)
            captureSession!.addInput(audioInput)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            
            videoFileOutput  = AVCaptureMovieFileOutput()

            
            if captureSession!.canAddOutput(stillImageOutput) && captureSession!.canAddOutput(videoFileOutput) {
                
                captureSession!.addOutput(stillImageOutput)
                captureSession!.addOutput(videoFileOutput)
                
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                previewView.layer.addSublayer(previewLayer!)
                
                captureSession!.startRunning()
            }
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer!.frame = previewView.bounds
    }
    
    
    func startVideo(){
        if isCapturingVideo {return}
        
        let recordingDelegate:AVCaptureFileOutputRecordingDelegate? = self

        let path = NSURL.init(fileURLWithPath: NSTemporaryDirectory()+"output.mov")
        videoFileOutput?.startRecordingToOutputFileURL(path, recordingDelegate: recordingDelegate)
        isCapturingVideo = true;
        
    }
    
    func stopVideo(){
        if !isCapturingVideo {return}
        
        videoFileOutput?.stopRecording()
        isCapturingVideo = false
    }
    
    func takePhoto(){
        if let videoConnection = stillImageOutput!.connectionWithMediaType(AVMediaTypeVideo) {
            videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    
                    let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                    print("Photo was taken");
                    
                    self.capturedImage = image;
                    self.pathVideo = NSURL()
                    self.performSegueWithIdentifier("goto_media", sender: self)
                }
            })
        }

    }

    @IBAction func pressTakePhoto(sender: AnyObject) {
        takePhoto()
    }
    
    
    @IBAction func pressTakeVideo(sender: UIGestureRecognizer) {
        //print("long: "+String(sender.state.rawValue))
        
        if(sender.state == .Began){
            startVideo()
            mainButton.backgroundColor = UIColor(red: 222/256, green: 68/256, blue: 68/256, alpha: 0.3)
        }
        if(sender.state == .Ended){
            stopVideo()
            mainButton.backgroundColor = UIColor.clearColor()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {

        let destinationVC = segue.destinationViewController as? MediaViewController
        destinationVC?.setup(capturedImage,path: pathVideo)
    }
    
}

extension ViewController: AVCaptureFileOutputRecordingDelegate
{
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
        print("Stop video: "+outputFileURL.path!)
        capturedImage = UIImage()
        pathVideo = outputFileURL.absoluteURL
        self.performSegueWithIdentifier("goto_media", sender: self)
        return
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        print("Start video");
        return
    }
}



